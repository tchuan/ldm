#!/bin/sh

gcc-4.8 -std=gnu99 -O2 -Wall -I./include -L./lib -lpthread -lrt -ljpeg -lm -lMVSDK ./src/matrix.c ./src/algorithm.c ./src/image.c ./src/device.c ./src/main.c -o ldm

mv ldm bin/ldm

#gcc -std=c99 -Wall -c ./src/matrix.c -o matrix.o
#gcc -std=c99 -Wall -c ./src/algorithm.c -o algorithm.o
#gcc -std=c99 -Wall -I./include -L./lib -ljpeg -c ./src/image.c -o image.o
#gcc -std=c99 -Wall -I./include -L./lib -lpthread -lrt -lMVSDK -c ./src/device.c -o device.o
#gcc -std=c99 -Wall -I./include -L./lib -lpthread -lrt -lMVSDK -ljpeg -c main.c -o main.o
#gcc -std=c99 -L./lib -lpthread -lrt -lMVSDK -ljpeg -lm matrix.o algorithm.o image.o device.o main.o -o ldm
