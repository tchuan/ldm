﻿#ifndef _DEVICE_H_
#define _DEVICE_H_

#define CAMERA_WIDTH 1280
#define CAMERA_HEIGHT 1024

int serial_open();

int serial_write(int fd, byte* buffer, int size);

int camera_init();

void camera_uninit(int camera);

int camera_get(int camera, mat_uchar* frame);

#endif
