#include <malloc.h>
#include <math.h>
#include "matrix.h"

mat_uchar* mat_uchar_alloc(int row, int column)
{
    mat_uchar* m = (mat_uchar*)malloc(sizeof(mat_uchar));
    m->row = row;
    m->column = column;
    m->data = (uchar*)malloc(row * column * sizeof(uchar));

    return m;
}

void mat_uchar_free(mat_uchar* m)
{
    free(m->data);
    free(m);
}

void mat_uchar_print(const mat_uchar* m, int r, int c)
{
    for (int i = 0; i < c && i < m->row; ++i) {
        for (int j = 0; j < c && j < m->column; ++j) {
            printf("%10d ", mat_uchar_get(m, i, j));
        }
        printf("\n");
    }
}


mat_int* mat_int_alloc(int row, int column)
{
    mat_int* m = (mat_int*)malloc(sizeof(mat_int));
    m->row = row;
    m->column = column;
    m->data = (int*)malloc(row * column * sizeof(int));

    return m;
}

void mat_int_print(const mat_int* m, int r, int c)
{
    for (int i = 0; i < c && i < m->row; ++i) {
        for (int j = 0; j < c && j < m->column; ++j) {
            printf("%10d ", mat_int_get(m, i, j));
        }
        printf("\n");
    }
}

void mat_int_free(mat_int* m)
{
    free(m->data);
    free(m);
}

mat* mat_alloc(int row, int column)
{
    mat* m = (mat*)malloc(sizeof(mat));
    m->row = row;
    m->column = column;
    m->data = (double*)malloc(row * column * sizeof(double));

    return m;
}

void mat_free(mat* m)
{
    free(m->data);
    free(m);
}

void mat_print(const mat* m, int r, int c)
{
    for (int i = 0; i < r && i < m->row; ++i) {
        for (int j = 0; j < c && j < m->column; ++j) {
            printf("%10.4f ", mat_get(m, i, j));
        }
        printf("\n");
    }
    printf("\n");
}

int inverse_matrix(double* A, int n)  //高斯求逆矩阵
{
    int i, j, k;
    double max, temp;
    double* t = (double*)malloc(n * n* sizeof(double));
    double* B = (double*)malloc(n * n* sizeof(double));

    for (i = 0; i < n; ++i) {                        //将A矩阵存放在临时矩阵t[n][n]中
        for (j = 0; j < n; ++j) {
            t[i*n + j] = *(A + i*n + j);
        }
    }

    for (i = 0; i < n; ++i) {                        //初始化B矩阵为单位阵 
        for (j = 0; j < n; ++j) {
            B[i*n + j] = (i == j) ? 1.0f : 0.0f;
        }
    }

    for (i = 0; i < n; ++i) {                        //寻找主元
        max = t[i*n + i];
        k = i;

        for (j = i + 1; j < n; ++j) {
            if (fabs(t[j*n + i]) > fabs(max)) {
                max = t[j*n + i];
                k = j;
            }
        }

        if (k != i) {                                //如果主元所在行不是第i行，进行行交换
            for (j = 0; j < n; ++j) {
                temp = t[i*n + j];
                t[i*n + j] = t[k*n + j];
                t[k*n + j] = temp;

                temp = B[i*n + j];
                B[i*n + j] = B[k*n + j];
                B[k*n + j] = temp;
            }
        }

        if (t[i*n + i] == 0) {                       //判断主元是否为0, 若是, 则矩阵A不是满秩矩阵,不存在逆矩阵
            return -1;
        }

        temp = t[i*n + i];                           //消去A的第i列除去i行以外的各行元素

        for (j = 0; j < n; ++j) {
            t[i*n + j] = t[i*n + j] / temp;          //主对角线上的元素变为1
            B[i*n + j] = B[i*n + j] / temp;          //伴随计算
        }

        for (j = 0; j < n; ++j) {                     //第0行->第n行
            if (j != i) {                             //不是第i行
                temp = t[j*n + i];

                for (k = 0; k < n; k++) {             //第j行元素 - i行元素*j列i行元素
                    t[j*n + k] = t[j*n + k] - t[i*n + k] * temp;
                    B[j*n + k] = B[j*n + k] - B[i*n + k] * temp;
                }
            }
        }
    }

    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            *(A + i*n + j) = B[i*n + j];
        }
    }

    free(B);
    free(t);

    return 0;
}
