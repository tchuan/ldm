﻿#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include "../include/CameraApi.h"
#include "matrix.h"
#include "device.h"

const int data_time = 100000;
const int data_interval = 200000;
const int MTU = 576;

int serial_open()
{
    int fd = -1;
    struct termios ios;

    fd = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (fd == -1) {
        printf("Error: Unable to open UART\n");
    }
    else {
        bzero(&ios, sizeof(ios));

        ios.c_cflag = (B115200 | CRTSCTS | CS8 | CLOCAL | CREAD);
        ios.c_iflag = IGNPAR;
        ios.c_oflag = 0;
        ios.c_lflag = 0;

        tcflush(fd, TCIFLUSH);
        tcsetattr(fd, TCSANOW, &ios);

        // raspberry pi has an bug
        write(fd, "Ready", 5);
    }

    return fd;
}

int serial_write(int fd, byte* buffer, int size)
{
    int send_size = 0;
    int count = 0;
    while (count < size) {
        send_size = write(fd, buffer, size - count > MTU ? MTU : size - count);
        if (send_size > 0) {
            buffer += send_size;
            count += send_size;
        }
        usleep(data_time);
    }

    usleep(data_interval);

    return count;
}

int camera_init()
{
    int camera_num = 1;
    int camera;
    tSdkCameraDevInfo camera_list;
    tSdkCameraCapbility camera_info;

    if (CameraSdkInit(1) != CAMERA_STATUS_SUCCESS) {
        printf("%s\n", "CameraSdkInit");
        return -1;
    }
    if (CameraEnumerateDevice(&camera_list, &camera_num) != CAMERA_STATUS_SUCCESS
        || camera_num <= 0) {
        printf("camera_num %d\n", camera_num);
        return -1;
    }
    if (CameraInit(&camera_list, -1, -1, &camera) != CAMERA_STATUS_SUCCESS) {
        printf("%s\n", "CameraInit");
        return -1;
    }
    if (CameraGetCapability(camera, &camera_info) != CAMERA_STATUS_SUCCESS) {
        printf("%s\n", "CameraGetCapability");
        return -1;
    }

    CameraSetTriggerMode(camera, SOFT_TRIGGER);
    CameraSetTriggerCount(camera, 1);

    CameraSetIspOutFormat(camera, CAMERA_MEDIA_TYPE_MONO8);

    int exposure = 10000;
    int gamma = 0;
    int contrast = 0;
    // Read Camera Param
    FILE* file = fopen("/home/pi/ldm/bin/camera.config", "r");
    if (file != NULL) {
        fseek(file, 0, SEEK_SET);

        fscanf(file, "%d", &exposure);
        fscanf(file, "%d", &gamma);
        fscanf(file, "%d", &contrast);

        fclose(file);
    }

    CameraSetAeState(camera, FALSE);
    CameraSetExposureTime(camera, exposure);

    CameraSetAnalogGain(camera, 2);

    if (gamma == 0 && contrast == 0) {// Line Mode
        CameraSetLutMode(camera, LUTMODE_PRESET);
        CameraSelectLutPreset(camera, 3);
    } else { // Param MODE
        CameraSetLutMode(camera, LUTMODE_PARAM_GEN);
        CameraSetGamma(camera, gamma);
        CameraSetContrast(camera, contrast);
    }

    if (CameraPlay(camera) != CAMERA_STATUS_SUCCESS) {
        printf("%s\n", "CameraPlay Failed\n");
        return -1;
    }

    return camera;
}

void camera_uninit(CameraHandle camera)
{
    CameraStop(camera);
    CameraUnInit(camera);
}

int camera_get(CameraHandle camera, mat_uchar* frame)
{
    tSdkFrameHead image_head;
    byte* buffer = NULL;

    if (CameraSoftTrigger(camera) != CAMERA_STATUS_SUCCESS) {
        printf("%s\n", "CameraSoftTrigger Failed\n");
        return -1;
    }

    if (CameraGetImageBuffer(camera, &image_head, &buffer, 2000) != CAMERA_STATUS_SUCCESS) {
        printf("%s\n", "CameraGetImageBuffer Failed\n");
        return -1;
    }
    if (CameraImageProcess(camera, buffer, frame->data, &image_head) != CAMERA_STATUS_SUCCESS) {
        return -1;
    }

    CameraSaveImage(camera, "/home/pi/ldm/last", frame->data, &image_head, FILE_BMP, 0);

    if (CameraReleaseImageBuffer(camera, buffer) != CAMERA_STATUS_SUCCESS) {
    }

    return 0;
}
