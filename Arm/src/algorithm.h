#ifndef _ALGORITHM_H_
#define _ALGORITHM_H_

// 初始化参数
int initializeParam();

// 标定，求畸变系数及像素宽度
int calibrate_camera(const mat_uchar* img);

// 求标准图像的畸变矩阵
void correct_distortion(const mat_uchar* img, mat_uchar* dst);

// 测量光斑位移
int measure_displacement(const mat_uchar* img, double* x0, double* y0);

#endif
