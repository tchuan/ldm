#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include "matrix.h"
#include "image.h"
#include "algorithm.h"

#define dist_length 10

// 标定用
const double grid_length = 14.2 / 14 * 10000.0;

//  图像畸变矫正及测量用
double g_magn = 0.0;
double dist_x[dist_length] = {0.0};
double dist_y[dist_length] = {0.0};

int initializeParam()
{
    FILE* file = fopen("/home/pi/ldm/bin/param.config", "r");
    if (file == NULL) {
        printf("read param.config failed!\n");
        return -1;
    }

    fseek(file, 0, SEEK_SET);
    fscanf(file, "%lf", &g_magn);
    for (size_t i = 0; i < dist_length; i++) {
        fscanf(file, "%lf", &dist_x[i]);
        fscanf(file, "%lf", &dist_y[i]);
    }
    fclose(file);

    return 0;
}

void search_cood(const mat_uchar* img,
                double wintx, double winty, mat_int* offx, mat_int* offy, mat* mask,
                double *_x, double *_y)
{
    const int max_iter = 5;
    int img_height = img->row;
    int img_width = img->column;

    // 临时变量
    double cIx = 0.0;
    double cIy = 0.0;
    double crIx = 0.0;
    double crIy = 0.0;
    double itIx = 0.0;
    double itIy = 0.0;
    double vIx[3] = { 0.0, 0.0, 0.0 };
    double vIy[3] = { 0.0, 0.0, 0.0 };
    int xmin = 0;
    int xmax = 0;
    int ymin = 0;
    int ymax = 0;

    for (int k = 0; k < max_iter; k++) {
        cIx = *_x;
        cIy = *_y;
        crIx = round(cIx);
        crIy = round(cIy);
        itIx = cIx - crIx;
        itIy = cIy - crIy;

        if (itIx > 0.0) {
            vIx[0] = itIx;
            vIx[1] = 1.0 - itIx;
            vIx[2] = 0.0;
        } else {
            vIx[0] = 0.0;
            vIx[1] = 1.0 + itIx;
            vIx[2] = -itIx;
        }
        if (itIy > 0.0) {
            vIy[0] = itIy;
            vIy[1] = 1.0 - itIy;
            vIy[2] = 0.0;
        } else {
            vIy[0] = 0.0;
            vIy[1] = 1.0 + itIy;
            vIy[2] = -itIy;
        }

        if (crIx - wintx - 2 < 1) {
            xmin = 1;
            xmax = wintx * 2 + 5;
        } else if (crIx + wintx + 2 > img_height) {
            xmin = img_height - wintx * 2 - 4;
            xmax = img_height;
        } else {
            xmin = crIx - wintx - 2;
            xmax = crIx + wintx + 2;
        }
        if (crIy - winty - 2 < 1) {
            ymin = 1;
            ymax = winty * 2 + 5;
        } else if (crIy + winty + 2 > img_width) {
            ymin = img_width - winty * 2 - 4;
            ymax = img_width;
        } else {
            ymin = crIy - winty - 2;
            ymax = crIy + winty + 2;
        }

        // 求卷积, 要先保存原图像
        int conv_height = xmax - xmin - 1;
        int conv_width = ymax - ymin + 1;
        mat* conv_x = mat_alloc(conv_height, conv_width);
        for (int i = xmin; i < xmax - 1; ++i) {
            for (int j = ymin - 1; j < ymax; ++j) {
                mat_set(conv_x, i-xmin, j-ymin+1,
                        mat_uchar_get(img, i-1, j)*vIx[2] + mat_uchar_get(img, i, j)*vIx[1] + mat_uchar_get(img, i+1, j)*vIx[0]);
            }
        }

        conv_width -= 2;
        mat* conv_y = mat_alloc(conv_height, conv_width);
        for (int i = 0; i < conv_height; ++i) {
            for (int j = 0; j < conv_width; ++j) {
                mat_set(conv_y, i, j, mat_get(conv_x, i, j)*vIy[2] + mat_get(conv_x, i, j+1)*vIy[1] + mat_get(conv_x, i, j+2)*vIy[0]);
            }
        }

        // 求梯度
        conv_height -= 2;
        conv_width -= 2;
        mat* gx = mat_alloc(conv_height, conv_width);
        mat* gy = mat_alloc(conv_height, conv_width);
        for (int i = 0; i < conv_height; i++) {
            for (int j = 0; j < conv_width; ++j) {
                mat_set(gx, i, j, (mat_get(conv_y, i+2, j+1) - mat_get(conv_y, i, j+1)) / 2.0);
                mat_set(gy, i, j, (mat_get(conv_y, i+1, j+2) - mat_get(conv_y, i+1, j)) / 2.0);
            }
        }

        double a = 0.0; // sum_gxx;
        double b = 0.0; // sum_gxy;
        double c = 0.0; // sum_gyy;
        double bb[2] = { 0.0, 0.0 };
        for (int i = 0; i < conv_height; ++i) {
            for (int j = 0; j < conv_width; ++j) {
                double px = cIx + mat_int_get(offx, i, j);
                double py = cIy + mat_int_get(offy, i, j);
                double gxx = mat_get(gx, i, j) * mat_get(gx, i, j) * mat_get(mask, i, j);
                double gyy = mat_get(gy, i, j) * mat_get(gy, i, j) * mat_get(mask, i, j);
                double gxy = mat_get(gx, i, j) * mat_get(gy, i, j) * mat_get(mask, i, j);
                bb[0] += gxx * px + gxy * py;
                bb[1] += gxy * px + gyy * py;
                a += gxx;
                b += gxy;
                c += gyy;
            }
        }
        double dt = a*c - b*b;
        *_x = (c*bb[0] - b*bb[1]) / dt;
        *_y = (a*bb[1] - b*bb[0]) / dt;

        mat_free(gx);
        mat_free(gy);
        mat_free(conv_x);
        mat_free(conv_y);
    }
}

int calibrate_camera(const mat_uchar* img)
{
    const int cood_height = 9;
    const int cood_width = 12;
    double* cood_x = (double*)malloc(cood_height*cood_width * sizeof(double));
    double* cood_y = (double*)malloc(cood_height*cood_width * sizeof(double));

    FILE* file = fopen("/home/raspberry/cood.config", "r");
    if (file == NULL) {
        printf("read cood.config failed!\n");
        return -1;
    }

    fseek(file, 0, SEEK_SET);
    for (size_t i = 0; i < cood_height * cood_width; i++) {
        fscanf(file, "%lf", cood_x + i);
        fscanf(file, "%lf", cood_y + i);
    }
    fclose(file);

    mat* cx = mat_alloc(cood_height, cood_width);
    mat* cy = mat_alloc(cood_height, cood_width);
    for (int j = 0; j < cood_width; ++j) {
        for (int i = 0; i < cood_height; ++i) {
            mat_set(cx, i, j, cood_x[j*cood_height+i]);
            mat_set(cy, i, j, cood_y[j*cood_height+i]);
        }
    }

    // 初始化窗口矩阵
    double wintx = round((cood_y[1 * 12] - cood_y[0]) / 3);
    double winty = wintx;
    mat_int *offx = mat_int_alloc(wintx * 2 + 1, winty * 2 + 1);
    mat_int *offy = mat_int_alloc(wintx * 2 + 1, winty * 2 + 1);
    mat *mask = mat_alloc(wintx * 2 + 1, winty * 2 + 1);
    for (int i = -wintx; i <= wintx; i++) {
        for (int j = -winty; j <= winty; j++) {
            mat_int_set(offx, i+wintx, j+winty, i);
            mat_int_set(offy, j+winty, i+wintx, i);

            double x = (double)i / wintx;
            double y = (double)j / winty;
            double v = exp(-x*x) * exp(-y*y);
            mat_set(mask, i+wintx, j+winty, v);
        }
    }

    // harris角点检测
    int max_iter = 5;
    for (int i = 0; i < cood_height; i++) {
        for (int j = 0; j < cood_width; j++) {
            double x = mat_get(cy, i, j);
            double y = mat_get(cx, i, j);
            for (int k = 0; k < max_iter; k++){
                search_cood(img, wintx, winty, offx, offy, mask, &x, &y);
            }

            mat_set(cy, i, j, x);
            mat_set(cx, i, j, y);
        }
    }

    // 标准坐标,
    int pl = cood_height/ 2;
    int pr = cood_width / 2;
    double xm = mat_get(cx, pl, pr);
    double ym = mat_get(cy, pl, pr);
    double sx = (mat_get(cx, pl, pr+1) - mat_get(cx, pl, pr-1))/2;
    double sy = (mat_get(cy, pl+1, pr) - mat_get(cy, pl-1, pr))/2;

    g_magn = grid_length / ((sx+sy)/2);

    int A_width = cood_height * cood_width;
    mat *xd = mat_alloc(1, A_width);
    mat *yd = mat_alloc(1, A_width);
    mat *dx = mat_alloc(1, A_width);
    mat *dy = mat_alloc(1, A_width);
    for (int i = 0; i < cood_height; ++i) {
        for (int j = 0; j < cood_width; ++j) {
            double temp_x = (xm - floor(cood_width / 2.0) * sx) + j*sx;
            double temp_y = (ym - floor(cood_height / 2.0) * sy) + i*sy;
            mat_set(xd, 0, j*cood_height+i, temp_x);
            mat_set(yd, 0, j*cood_height+i, temp_y);
            mat_set(dx, 0, j*cood_height+i, mat_get(cx, i, j) - temp_x);
            mat_set(dy, 0, j*cood_height+i, mat_get(cy, i, j) - temp_y);
        }
    }

    mat* A = mat_alloc(10, A_width);
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < A_width; ++j) {
            mat_set(A, 0, j, 1.0);
            mat_set(A, 1, j, mat_get(xd, 0, j));
            mat_set(A, 2, j, mat_get(yd, 0, j));
            mat_set(A, 3, j, mat_get(xd, 0, j)*mat_get(xd, 0, j));
            mat_set(A, 4, j, mat_get(xd, 0, j)*mat_get(yd, 0, j));
            mat_set(A, 5, j, mat_get(yd, 0, j)*mat_get(yd, 0, j));
            mat_set(A, 6, j, mat_get(xd, 0, j)*mat_get(xd, 0, j)*mat_get(xd, 0, j));
            mat_set(A, 7, j, mat_get(xd, 0, j)*mat_get(xd, 0, j)*mat_get(yd, 0, j));
            mat_set(A, 8, j, mat_get(xd, 0, j)*mat_get(yd, 0, j)*mat_get(yd, 0, j));
            mat_set(A, 9, j, mat_get(yd, 0, j)*mat_get(yd, 0, j)*mat_get(yd, 0, j));
        }
    }

    // distx = dx*At*(AAt)-1
    double* dxAt = (double*)malloc(10 * sizeof(double));
    double* dyAt = (double*)malloc(10 * sizeof(double));
    for (int i = 0; i < 10; i++) {
        double tempx = 0.0;
        double tempy = 0.0;
        for (int j = 0; j < cood_height*cood_width; j++) {
            tempx += dx->data[j] * mat_get(A, i, j);
            tempy += dy->data[j] * mat_get(A, i, j);
        }
        dxAt[i] = tempx;
        dyAt[i] = tempy;
    }

    mat* AAt = mat_alloc(10, 10);
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; ++j) {
            double temp = 0.0;
            for (int k = 0; k < A_width; k++) {
                temp += mat_get(A, i, k) * mat_get(A, j, k);
            }
            mat_set(AAt, i, j, temp);
        }
    }

    inverse_matrix(AAt->data, AAt->row);

    for (int i = 0; i < 10; i++) {
        double tempx = 0.0;
        double tempy = 0.0;
        for (int j = 0; j < 10; j++) {
            tempx += dxAt[j] * mat_get(AAt, j, i);
            tempy += dyAt[j] * mat_get(AAt, j, i);
        }
        dist_x[i] = tempx;
        dist_y[i] = tempy;
    }
    /*
    for (size_t i = 0; i < 10; i++) {
        printf("%10.6f ", dist_x[i]);
    }
    printf("\n");
    for (size_t i = 0; i < 10; i++) {
        printf("%10.6f ", dist_y[i]);
    }
    printf("\n");
    printf("%10.6f\n", g_magn);
    */

    mat_int_free(offx);
    mat_int_free(offy);
    mat_free(mask);
    mat_free(cx);
    mat_free(cy);
    mat_free(xd);
    mat_free(yd);
    mat_free(dx);
    mat_free(dy);
    mat_free(A);
    mat_free(AAt);
    free(dxAt);
    free(dyAt);

    return 0;
}

// 求标准图像的畸变矩阵
void correct_distortion(const mat_uchar* img, mat_uchar* dst)
{
    int height = img->row;
    int width = img->column;
    for (int i = 1; i <= height; i++) {
        for (int j = 1; j <= width; j++) {
            double x = j + (dist_x[0] + dist_x[1] * j + dist_x[2] * i + dist_x[3] * j*j + dist_x[4] * i*j + dist_x[5] * i*i + dist_x[6] * j*j*j + dist_x[7] * j*j*i + dist_x[8] * j*i*i + dist_x[9] * i*i*i);
            double y = i + (dist_y[0] + dist_y[1] * j + dist_y[2] * i + dist_y[3] * j*j + dist_y[4] * i*j + dist_y[5] * i*i + dist_y[6] * j*j*j + dist_y[7] * j*j*i + dist_y[8] * j*i*i + dist_y[9] * i*i*i);

            int Ax = floor(x);
            int Ay = floor(y);
            int Bx = Ax;
            int By = Ay + 1;
            int Cx = Ax + 1;
            int Cy = Ay + 1;
            int Dx = Ax + 1;
            int Dy = Ay;
            if (!(Ax > 1 && Ay > 1 && Bx > 1 && By > 1 && Cx > 1 && Cy > 1 && Dx > 1 && Dy > 1 &&
                Ax < width && Ay < height && Bx < width && By < height && Cx < width && Cy < height && Dx < width && Dy < height))
                continue;

            double oo = mat_uchar_get(img, Ay, Ax) + (mat_uchar_get(img, By, Bx) - mat_uchar_get(img, Ay, Ax)) * (y - Ay);
            double ii = mat_uchar_get(img, Dy, Dx) + (mat_uchar_get(img, Cy, Cx) - mat_uchar_get(img, Dy, Dx)) * (y - Ay);

            uchar value = (uchar)(oo + (ii - oo)*(x - Ax));
            mat_uchar_set(dst, i - 1, j - 1, value);
        }
    }
}

// 测量光斑位移
int measure_displacement(const mat_uchar* img, double* x, double* y)
{
    int threshold = 128;
    double total = 0;
    double totalX = 0;
    double totalY = 0;
    for (size_t i = 1; i <= img->row; i++) {
        for (size_t j = 1; j <= img->column; j++) {
            int data = mat_uchar_get(img, i-1, j-1);
            data = data < threshold ? 0 : data;
            total += data;          // 总灰度
            totalX += data * j;     // 对Y轴静矩
            totalY += data * i;     // 对X轴静矩
        }
    }

    if (total <= 0)
        return -1;

    *x = totalX / total * g_magn;
    *y = totalY / total * g_magn;
    return 0;
}
