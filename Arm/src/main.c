#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>

#include "matrix.h"
#include "device.h"
#include "image.h"
#include "algorithm.h"

int          fd = -1;       // serial port
int          camera = -1;   // camera_id
double       xx0;
double       yy0;
mat_uchar*   frame_buffer = NULL;
mat_uchar*   correct_buffer = NULL;
uchar*       jpg_buffer = NULL;

void* send_image_thread(void* arg)
{
    if (camera_get(camera, frame_buffer) < 0) {
        pthread_exit(NULL);
    }

    unsigned long jpg_size =
        bmp_to_jpeg(frame_buffer->data, &jpg_buffer, frame_buffer->column, frame_buffer->row, 1, 70);
    if (jpg_size <= 0) {
        printf("convert bmp to jpeg failed!\n");
        return 0;
    }
    printf("jpeg size is %lu\n", jpg_size);

    // Send Jpg Start
    static byte header[8];
    header[0] = 0xD9;
    header[1] = 0x03;
    header[2] = 0x01;
    header[3] = 0x01;
    header[4] = (jpg_size >> 24) & 0xFF;
    header[5] = (jpg_size >> 16) & 0xFF;
    header[6] = (jpg_size >> 8) & 0xFF;
    header[7] = (jpg_size >> 0) & 0xFF;

    int count = serial_write(fd, header, sizeof(header));
    printf("Start Command: %d sended\n", count);

    count = serial_write(fd, jpg_buffer, jpg_size);
    printf(" %d sended\n", count);
    // Send Jpg End
    header[3] = 0x02;
    count = write(fd, header, sizeof(header));
    printf("Stop Command: %d sended\n", count);

    return 0;
}

void* calibrate_thread(void* arg)
{
    initializeParam();

    static byte header[4];
    header[0] = 0xD9;
    header[1] = 0x03;
    header[2] = 0x01;
    header[3] = 0x03;

    int count = serial_write(fd, header, sizeof(header));
    printf("Calibration Command: %d sended\n", count);

    return 0;
}

// arg == 0: initialize displacement
// arg == 1: meature
void* meature_thread(void* arg)
{
    if (camera_get(camera, frame_buffer) < 0) {
        printf("get camera frame buffer failed!\n");
        pthread_exit(NULL);
    }

    correct_distortion(frame_buffer, correct_buffer);

    double x;
    double y;
    int xx;
    int yy;
    if (measure_displacement(correct_buffer, &x, &y) < 0) {
        printf("measure failed! image is too dark\n");
        pthread_exit(NULL);
    }
    if (*(int*)arg == 0) {
        xx0 = x;
        yy0 = y;
        xx = (int)(xx0*10000.0);
        yy = (int)(yy0*10000.0);
        printf("xx0 = %f, yy0 = %f\n", xx0 / 1000.0, yy0 / 1000.0);
    } else {
        xx = (int)((x-xx0)*10000.0);
        yy = (int)((y-yy0)*10000.0);
        printf("x = %f, y = %f\n", (x - xx0) / 1000.0, (y - yy0) / 1000.0);
    }

    // send meature displacement
    static byte header[12];
    header[0] = 0xD9;
    header[1] = 0x03;
    header[2] = 0x01;
    header[3] = 0x08;
    header[4] = xx & 0xFF;
    header[5] = (xx >> 8) & 0xFF;
    header[6] = (xx >> 16) & 0xFF;
    header[7] = (xx >> 24) & 0xFF;
    header[8] = yy & 0xFF;
    header[9] = (yy >> 8) & 0xFF;
    header[10] = (yy >> 16) & 0xFF;
    header[11] = (yy >> 24) & 0xFF;

    int count = serial_write(fd, header, sizeof(header));
    printf("Measure Complete Command: %d sended\n", count);

    return 0;
}

void process_data()
{
    pthread_t pid;
    byte      buf[64];
    int       len = -1;
    int arg = 0;

    calibrate_thread(NULL);

    while (1)
    {
        usleep(200000);

        len = read(fd, (void*)buf, sizeof(buf));

        if (len > 0)
            printf("\n");
        for (int i = 0; i < len; i++)
            printf("%02x ", buf[i]);

        if (len < 4 || !(buf[0] == 0xD9 && buf[1] == 0x03 && buf[2] == 0x01))
            continue;

        switch(buf[3]) {
            case 0x01: // 标定预览
            //    pthread_create(&pid, NULL, send_image_thread, NULL);
                break;
            case 0x02: // 标定
                pthread_create(&pid, NULL, calibrate_thread, NULL);
                break;
            case 0x03: // 测量预览
            //    pthread_create(&pid, NULL, send_image_thread, NULL);
                break;
            case 0x04: // 初始化测量
                arg = 0;
                pthread_create(&pid, NULL, meature_thread, (void*)&arg);
                break;
            case 0x05: // 测量
                arg = 1;
                pthread_create(&pid, NULL, meature_thread, (void*)&arg);
                break;
            case 0xFF: // 结束测量
                break;
        }
    }
}

int main()
{
    fd = serial_open();
    if (fd < 0) {
        printf("error: open serail port failed!\n");
        return -1;
    }

    camera = camera_init();
    if (camera < 0) {
        printf("error: init camera failed!\n");
        return -1;
    }

    frame_buffer = mat_uchar_alloc(CAMERA_HEIGHT, CAMERA_WIDTH);
    correct_buffer = mat_uchar_alloc(CAMERA_HEIGHT, CAMERA_WIDTH);
    jpg_buffer = (byte*)malloc(CAMERA_HEIGHT * CAMERA_WIDTH * sizeof(jpg_buffer));

    process_data();

    camera_uninit(camera);
    close(fd);
    free(frame_buffer);
    free(jpg_buffer);

    return 0;
}
