#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <assert.h>

typedef unsigned char uchar;
typedef unsigned char byte;

typedef struct {
    int row;
    int column;
    uchar *data;
} mat_uchar;

typedef struct {
    int row;
    int column;
    int *data;
} mat_int;

typedef struct {
    int row;
    int column;
    double *data;
} mat;

// mat_double_function
mat* mat_alloc(int row, int column);

inline double mat_get(const mat* m, int i, int j)
{
    return m->data[i * m->column + j];
}

inline void mat_set(mat* m, int i, int j, double v)
{
    m->data[i * m->column + j] = v;
}

void mat_free(mat* m);

//高斯求逆矩阵
int inverse_matrix(double* A, int n);

void mat_print(const mat* m, int r, int c);

// mat_uchar_function
mat_uchar* mat_uchar_alloc(int row, int column);

inline uchar mat_uchar_get(const mat_uchar* m, int i, int j)
{
    return m->data[i * m->column + j];
}

inline void mat_uchar_set(mat_uchar* m, int i, int j, uchar v)
{
    m->data[i * m->column + j] = v;
}

void mat_uchar_free(mat_uchar* m);

void mat_uchar_print(const mat_uchar* m, int r, int c);

// mat_int_function
mat_int* mat_int_alloc(int row, int column);

inline int mat_int_get(const mat_int* m, int i, int j)
{
    assert(i >= 0 && i < m->row && j >= 0 && j < m->column);
    return m->data[i * m->column + j];
}

inline void mat_int_set(mat_int* m, int i, int j, int v)
{
    assert(i >= 0 && i < m->row && j >= 0 && j < m->column);
    m->data[i * m->column + j] = v;
}

void mat_int_free(mat_int* m);

void mat_int_print(const mat_int* m, int r, int c);

#endif
