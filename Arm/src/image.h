﻿#ifndef _IMAGE_H_
#define _IMAGE_H_

#include "matrix.h"

int read_bmp(char* file, mat_uchar* m);

int write_bmp(char* file, uchar* src);

unsigned long bmp_to_jpeg(byte* src, byte** dst,  int width, int height, int depth, int quality);

#endif
