#include <stdio.h>
#include <malloc.h>
#include "../include/jpeglib.h"
#include "image.h"

#define bmp_width 1280
#define bmp_height 1024
#define bmp_header_len 54

uchar bmp_header[bmp_header_len];

inline int getBuf(int x, int y, uchar* buf)
{
    return buf[1280 * (1024-x-1) * 3 + y * 3];
}

inline void setBuf(uchar *buf, int x, int y, uchar v)
{
    buf[1280 * (1024-x-1) * 3 + y * 3 + 0] = v;
    buf[1280 * (1024-x-1) * 3 + y * 3 + 1] = v;
    buf[1280 * (1024-x-1) * 3 + y * 3 + 2] = v;
}

int read_bmp(char* file_name, mat_uchar* m)
{
    uchar *img = (uchar*)malloc(bmp_height * bmp_width * 3 * sizeof(uchar));
    FILE* file = fopen(file_name, "rb");
    if (file == NULL) {
        printf("read bmp file failed!\n");
        return -1;
    }
    fseek(file, 0, SEEK_SET);
    fread(bmp_header, sizeof(uchar), bmp_header_len, file);
    fread(img, sizeof(uchar), bmp_height*bmp_width*3, file);

    for (int i = 0; i < bmp_height; ++i) {
        for (int j = 0; j < bmp_width; ++j) {
            mat_uchar_set(m, i, j, getBuf(i, j, img));
        }
    }

    free(img);
    return 0;
}

int write_bmp(char* file_name, uchar* src)
{
    FILE* file = fopen(file_name, "wb");
    if (file == NULL) {
        printf("create bmp file failed!\n");
        return -1;
    }
    fseek(file, 0, SEEK_SET);
    fseek(file, 0, SEEK_SET);
    fwrite(bmp_header, sizeof(uchar), bmp_header_len, file);
    fwrite(src, sizeof(uchar), bmp_height*bmp_width*3, file);
    fflush(file);
    fclose(file);

    return 0;
}

unsigned long bmp_to_jpeg(byte* src, byte** dst, int width, int height, int depth, int quality)
{
    // FILE* file;
    struct jpeg_error_mgr jerr;
    struct jpeg_compress_struct cinfo;
    JSAMPROW row_pointer[1];
    int row_stride;
    unsigned long size;

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_compress(&cinfo);

    // file = fopen(file_name, "wb");
    // if (file == NULL)
    //    return -1;

    // jpeg_stdio_dest(&cinfo, file);
    jpeg_mem_dest(&cinfo, dst, &size);

    cinfo.image_width = width;
    cinfo.image_height = height;
    cinfo.input_components = depth;
    cinfo.in_color_space = (depth == 3) ? JCS_BG_RGB : JCS_GRAYSCALE;

    jpeg_set_defaults(&cinfo);
    jpeg_set_quality(&cinfo, quality, TRUE);

    jpeg_start_compress(&cinfo, TRUE);

    row_stride = width * cinfo.input_components;
    while (cinfo.next_scanline < cinfo.image_height)
    {
        row_pointer[0] = &src[cinfo.next_scanline * row_stride];
        (void)jpeg_write_scanlines(&cinfo, row_pointer, 1);
    }

    jpeg_finish_compress(&cinfo);
    jpeg_destroy_compress(&cinfo);

    // fclose(file);

    return size;
}
