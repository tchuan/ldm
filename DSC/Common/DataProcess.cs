﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using DSC.Model;

namespace DSC.Common
{
    class DataProcess
    {
        static public void ProcessRecord()
        {
            while (true)
            {
                Thread.Sleep(100);

                var data = new DataRecord();
                if (Dtu.do_read_proc(ref data, null, false) < 0)
                    continue;
                switch (data.DataType)
                {
                    case 0x01: // DTU注册包，心跳包, 界面刷新DTU信息
                        break;
                    case 0x02: // DTU请求注销包
                        break;
                    case 0x04: // DTU上次收到DSC的错误包，对用户来说意味着配置参数出错
                        break;
                    case 0x05: // DTU已经接收到DSC发送的用户数据包
                        break;
                    case 0x06: // DTU收到do_disconnect_ppp_link的请求
                        break;
                    case 0x07: // DTU收到do_stop_send_data的请求
                        break;
                    case 0x08: // DTU收到do_start_send_data的请求
                        break;
                    case 0x09: // DTU数据包
                        // TODO：可能缓冲区有多条数据？
                        if (!VerifyData(data.DataBuf, data.DataLen))
                            continue;
                        Task.Run(() => ProcessRecvData(data.DataBuf, data.DataLen));
                        break;
                    case 0x0b: // DTU返回参数查询结果，使用GetParam读取参数
                        break;
                    case 0x0d: // DTU参数配置成功
                        break;
                    default:
                        break;
                }
            }
        }

        public static bool VerifyData(byte[] buf, int len)
        {
            // 数据长度小于7，数据头无效， 数据段长度不对，无效
            return len >= 4 && (buf[0] == 0xD9 && buf[1] == 0x03 && buf[2] == 0x01);
        }

        public static void ProcessRecvData(byte[] buf, int len)
        {
            var id = (int)buf[2];
            switch (buf[3])
            {
                case 0x03: // 标定成功
                    Messenger.Default.Send(new MsgArgs(id, "标定成功"));
                    break;
                case 0x04: // 标定失败
                    Messenger.Default.Send(new MsgArgs(id, "标定失败"));
                    break;
                case 0x05: // 测量成功
                    Messenger.Default.Send(new MsgArgs(id, "测量成功"));
                    break;
                case 0x06: // 测量失败
                    Messenger.Default.Send(new MsgArgs(id, "测量失败"));
                    break;
                case 0x08: // 测量结果
                    var x = buf[4] | (buf[5] << 8) | (buf[6] << 16) | (buf[7] << 24);
                    var y = buf[8] | (buf[9] << 8) | (buf[10] << 16) | (buf[11] << 24);
                    var msg = $"X: {x/10000.0/1000.0}mm, Y: {y/10000.0/1000.0}mm";
                    Messenger.Default.Send(new MsgArgs(id, msg));

                    break;
            }
        }
    }
}
