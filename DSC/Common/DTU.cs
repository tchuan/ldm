﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace DSC.Common
{
    public struct WorkMode
    {
        public const int Block = 0;    // 阻塞模式
        public const int NonBlock = 1; // 非阻塞模式
        public const int Message = 2;  // 消息模式
    }

    public struct Protocal
    {
        public const int UDP = 0;
        public const int TCP = 1;
    }

    public struct UserInfo
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
        public string UserId;
        public uint SinAddr;
        public ushort SinPort;
        public uint LocalAddr;
        public ushort LocalPort;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string LoginDate;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public byte[] UpdateDate;
        public byte Status;
    }

    public struct DataRecord
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
        public string UserId;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string RecvDate;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
        public byte[] DataBuf;
        public ushort DataLen;
        public byte DataType;
    }

    class Dtu
    {
        [DllImport("wcomm_dll.dll")]
        public static extern int SetWorkMode(int workMode);

        [DllImport("wcomm_dll.dll")]
        public static extern int SelectProtocol(int protocol);

        [DllImport("wcomm_dll.dll")]
        public static extern int SetCustomIP(uint ip);

        [DllImport("wcomm_dll.dll")]
        public static extern int start_net_service(IntPtr wnd, uint msgType, int serverPort, StringBuilder msg);

        [DllImport("wcomm_dll.dll")]
        public static extern int stop_net_service(StringBuilder msg);

        [DllImport("wcomm_dll.dll")]
        public static extern uint get_max_user_amount();

        [DllImport("wcomm_dll.dll")]
        public static extern uint get_online_user_amount();

        [DllImport("wcomm_dll.dll")]
        public static extern int get_user_info(string userId, ref UserInfo info);

        [DllImport("wcomm_dll.dll")]
        public static extern int get_user_at(uint index, ref UserInfo info);

        [DllImport("wcomm_dll.dll")]
        public static extern int do_read_proc(ref DataRecord data, StringBuilder msg, bool reply);

        [DllImport("wcomm_dll.dll")]
        public static extern int do_send_user_data(string userId, byte[] data, uint len, StringBuilder mess);

        [DllImport("wcomm_dll.dll")]
        public static extern int do_close_one_user2(string userId, StringBuilder msg);

        [DllImport("wcomm_dll.dll")]
        public static extern int do_close_all_user2(StringBuilder msg);

        [DllImport("wcomm_dll.dll")]
        public static extern void InvokeParamUI(IntPtr hParentWnd);
    }
}
