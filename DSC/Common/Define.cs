﻿namespace DSC.Common
{
    /*
     * 传感器类型： 4.正弦
     *            7.双倾角
     *            8.水准仪
     *           10.位移计
     */
    /*
     * 测点类型： 2,裂缝
     *          3,挠度
     *          4,应变
     *          5,温度
     *          6,倾角
     *          8,相对位移
     */

    public enum SubstationStatus
    {
        OffLine,            // 离线
        OnLine,             // 在线
        SamplingStarted,    // 采样开始
        SamplingStopped,    // 采样停止
        ProcessData,        // 收发数据
        LowBattery          // 电量低
    }

    public class MsgArgs
    {
        public int No;

        public string Msg;

        public MsgArgs(int no, string msg)
        {
            No = no;
            Msg = msg;
        }
    }

    public class BufArgs
    {
        public int No;

        public byte[] Buffer;

        public int Length;

        public BufArgs(int no, byte[] buf, int len)
        {
            No = no;
            Buffer = buf;
            Length = len;
        }
    }
}
