﻿using System;
using System.Globalization;
using System.Windows.Data;
using DSC_CCRDI.ViewModel;

namespace DSC_CCRDI.Converter
{
    [ValueConversion(typeof(SubstationStatus), typeof(bool))]
    class StatusToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = (SubstationStatus) value;
            var param = (string) parameter;
            if (param.Equals("0") 
                && (status == SubstationStatus.ParamSucceed || status == SubstationStatus.SamplingStop))
                return true;
            if (param.Equals("1") && status == SubstationStatus.SamplingStart)
                return true;
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
