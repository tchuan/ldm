﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using DSC.Common;

namespace DSC.Converter
{
    [ValueConversion(typeof (SubstationStatus), typeof (Brush))]
    internal class StatusToBrushColorConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((SubstationStatus)value)
            {
                case SubstationStatus.OffLine:
                    return Brushes.Gray;
                case SubstationStatus.OnLine:
                    return Brushes.LightGreen;
                case SubstationStatus.SamplingStarted:
                    return Brushes.Blue;
                case SubstationStatus.ProcessData:
                    return Brushes.Yellow;
                default:
                    return Brushes.Red;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}