﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DSC.Converter
{
    [ValueConversion(typeof (int), typeof (bool))]
    internal class SampleTypeToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return false;
            var param = parameter.ToString();
            var val = value.ToString();
            return param == val;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = (bool) value;
            if (val)
                return int.Parse(parameter.ToString());
            return null;
        }
    }
}
