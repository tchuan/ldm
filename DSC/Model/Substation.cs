﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DSC.Model
{
    public class Substation
    {
        public int No { get; set; }     // 分站编号

        public string Mobile { get; set; }

        public string Bridge { get; set; }

        public string DataBase { get; set; }

        public bool IsParamSet { get; set; }

        public bool IsSampling { get; set; }

        [XmlIgnore]
        public static Dictionary<int, Substation> SubstationDict { get; set; }

        static Substation()
        {
            SubstationDict = new Dictionary<int, Substation>();
        }
    }
}
