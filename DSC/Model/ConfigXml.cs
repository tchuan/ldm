﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DSC.Model
{
    [XmlRoot("Substations")]
    public class ConfigXml
    {
        public int Port { get; set; }

        [XmlElement(ElementName = "Substation")]
        public List<Substation> Items { get; set; }

        public ConfigXml()
        {
            Items = new List<Substation>();
        }
    }
}
