﻿using System.Windows;
using System.Windows.Interop;
using DSC.Common;
using DSC.ViewModel;

namespace DSC.View
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var viewModel = new MainWindowViewModel();
            Closing += viewModel.OnWindowClosing;
            Loaded += viewModel.OnWindowLoaded;

            DataContext = viewModel;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            Dtu.InvokeParamUI(new WindowInteropHelper(this).Handle);
        }
    }
}
