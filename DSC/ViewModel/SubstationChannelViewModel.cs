﻿using GalaSoft.MvvmLight;

namespace DSC_CCRDI.ViewModel
{
    class SubstationChannelViewModel : ViewModelBase
    {
        private bool _active;

        public int Id { get; set; }

        public bool Active
        {
            get { return _active; }
            set { _active = value; RaisePropertyChanged("Active"); }
        }

        public int LinkId { get; set; }

        public string MeaPtNum { get; set; }

        public string SensorNum { get; set; }

        public int MeatureType { get; set; }

        public int SensorType { get; set; }

        public double F0 { get; set; }

        public double T0 { get; set; }

        public double Data0 { get; set; }

        public string InitialTime { get; set; }

        public string SetTime { get; set; }

        public double Kt { get; set; }

        public double K { get; set; }

        public double A { get; set; }

        public double B { get; set; }

        public double C { get; set; }
    }
}
