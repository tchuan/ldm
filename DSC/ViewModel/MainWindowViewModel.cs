﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using DSC.Common;
using DSC.Model;

namespace DSC.ViewModel
{
    public class SubstationViewModel : ViewModelBase
    {
        private string _loginTime;
        private DateTime _refreshTime;
        private SubstationStatus _status;

        public SubstationStatus Status
        {
            get { return _status; }
            set { _status = value; RaisePropertyChanged(() => Status); }
        }

        public int No { get; set; }     // 分站编号

        public string Mobile { get; set; }

        public string LoginTime
        {
            get { return _loginTime; }
            set { _loginTime = value; RaisePropertyChanged(() => LoginTime); }
        }

        public DateTime RefreshTime
        {
            get { return _refreshTime; }
            set { _refreshTime = value; RaisePropertyChanged(() => RefreshTime); }
        }
    }

    class MainWindowViewModel : ViewModelBase
    {
        private static readonly byte[] CalibratePreCmd= { 0xD9, 0x03, 0x01, 0x01 };
        private static readonly byte[] CalibrateCmd = { 0xD9, 0x03, 0x01, 0x02 };
        private static readonly byte[] MeaturePreCmd = { 0xD9, 0x03, 0x01, 0x03 };
        private static readonly byte[] MeatureInitCmd = { 0xD9, 0x03, 0x01, 0x04 };
        private static readonly byte[] MeatureCmd = { 0xD9, 0x03, 0x01, 0x05 };
        private static readonly byte[] StopCmd = { 0xD9, 0x03, 0x01, 0xFF };

        private ushort _port = 5001;
        private bool _serviceStarted;
        private string _buttonText;
        private Brush _buttonColor;
        private string _messageSend;
        private string _messageRecevied;

        #region Property
        public ObservableCollection<SubstationViewModel> SubstationList { get; set; }

        public SubstationViewModel SelectedSubstation { get; set; }

        public ushort Port
        {
            get { return _port; }
            set { _port = value; RaisePropertyChanged(); }
        }

        public string ButtonText
        {
            get { return _buttonText; }
            set { _buttonText = value; RaisePropertyChanged(); }
        }

        public Brush ButtonColor
        {
            get { return _buttonColor; }
            set { _buttonColor = value; RaisePropertyChanged(); }
        }

        public bool ServiceStarted
        {
            get { return _serviceStarted; }
            set { _serviceStarted = value; RaisePropertyChanged(); } 
        }

        public String MessageSend
        {
            get { return _messageSend; }
            set
            {
                _messageSend = DateTime.Now.ToString("G") + ": " + value + Environment.NewLine + _messageSend;
                if (_messageSend.Length > 1024)
                    _messageSend.Remove(256);
                RaisePropertyChanged();
            }
        }

        public String MessageRecevied
        {
            get { return _messageRecevied; }
            set
            {
                _messageRecevied = DateTime.Now.ToString("G") + ": " + value + Environment.NewLine + _messageRecevied;
                if (_messageRecevied.Length > 1024)
                    _messageRecevied.Remove(256);
                RaisePropertyChanged();
            }
        }

        public ICommand StartServiceCommand => new RelayCommand(SetService);

        public ICommand CalibratePreCommand
        {
            get { return new RelayCommand(() => SendCommand(1)); }
        }

        public ICommand CalibrateCommand
        {
            get { return new RelayCommand(() => SendCommand(2)); }
        }

        public ICommand MeaturePreCommand
        {
            get { return new RelayCommand(() => SendCommand(3)); }
        }

        public ICommand MeatureInitCommand
        {
            get { return new RelayCommand(() => SendCommand(4)); }
        }
        public ICommand MeatureCommand
        {
            get { return new RelayCommand(() => SendCommand(5)); }
        }
        public ICommand StopCommand
        {
            get { return new RelayCommand(() => SendCommand(0)); }
        }

        #endregion

        public MainWindowViewModel()
        {
            SubstationList = new ObservableCollection<SubstationViewModel>();
            ButtonText = "启动服务";
            ButtonColor = Brushes.LawnGreen;

            Task.Factory.StartNew(DataProcess.ProcessRecord, TaskCreationOptions.LongRunning);
            Messenger.Default.Register<MsgArgs>(this, UpdateUI);
        }

        public void OnWindowLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            LoadConfig();
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            if (_serviceStarted)
            {
                MessageBox.Show(@"请先停止服务！");
                e.Cancel = true;
                return;
            }
            if (MessageBox.Show(@"是否保存配置文件？", @"警告", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                SaveConfig();
            }
        }

        private void LoadConfig()
        {
            const string fileName = "分站配置.xml";
            if (!File.Exists(fileName))
                return;

            Substation.SubstationDict = new Dictionary<int, Substation>();
            var xml = new XmlSerializer(typeof(ConfigXml));
            // 读取配置文件
            var file = XmlReader.Create("分站配置.xml");
            var stations = xml.Deserialize(file) as ConfigXml;
            if (stations != null)
            {
                Port = (ushort)stations.Port;
                // 从数据库读取分站编号等信息
                foreach (var o in stations.Items)
                {
                    Substation.SubstationDict[o.No] = o;
                    SubstationList.Add(new SubstationViewModel
                    {
                        No = o.No,
                        Mobile = o.Mobile
                    });
                }
            }
            file.Close();
        }

        private void SaveConfig()
        {
            var stations = new ConfigXml { Port = _port };
            stations.Items.AddRange(Substation.SubstationDict.Values.ToList());

            try
            {
                var settings = new XmlWriterSettings
                {
                    Indent = true,
                    IndentChars = "    ",
                    OmitXmlDeclaration = true
                };
                var file = XmlWriter.Create("分站配置.xml", settings);
                var xml = new XmlSerializer(typeof (ConfigXml));
                xml.Serialize(file, stations);
                file.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void UpdateUI(MsgArgs msg)
        {
            foreach (var t in SubstationList)
            {
                t.Status = SubstationStatus.ProcessData;
                MessageRecevied = msg.Msg;
                return;
            }
        }

        private void SetService()
        {
            if (_serviceStarted)
            {
                if (!StopService())
                    return;
                ButtonText = "开始服务";
                ButtonColor = Brushes.LawnGreen;
            }
            else
            {
                if (!StartService())
                    return;
                ButtonText = "停止服务";
                ButtonColor = Brushes.Red;
            }
        }

        private bool StartService()
        {
            Dtu.SetWorkMode(WorkMode.NonBlock);
            Dtu.SelectProtocol(Protocal.UDP);

            var msg = new StringBuilder(1024);
            if (Dtu.start_net_service(new IntPtr(0), 0, Port, msg) != 0)
            {
                MessageBox.Show(msg.ToString());
                return false;
            }

            MessageSend = "数据中心服务启动！";
            ServiceStarted = true;

            // 轮询在线的DTU
            Task.Factory.StartNew(() =>
            {
                while (ServiceStarted)
                {
                    UpdateSubstations();
                    Thread.Sleep(3000);
                }
            }, TaskCreationOptions.LongRunning);

            return true;
        }

        private bool StopService()
        {
            if (Substation.SubstationDict.Values.Any(station => station.IsSampling))
            {
                if (MessageBox.Show(@"有分站正在采集数据，是否强制停止", "", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                    return false;
            }

            var msg = new StringBuilder(1024);
            if (ServiceStarted && Dtu.stop_net_service(msg) == 0)
            {
                MessageSend = "数据中心服务结束！";
                ServiceStarted = false;
            }
            else
            {
                MessageBox.Show("停止服务失败" + msg);
            }
            return true;
        }

        private void SendCommand(int type)
        {
            if (SelectedSubstation == null)
                return;
            var station = Substation.SubstationDict[SelectedSubstation.No];

            var cmd = StopCmd;
            var cmdType = "结束";
            switch (type)
            {
                case 1:
                    cmd = CalibratePreCmd;
                    cmdType = "预览";
                    break;
                case 2:
                    cmd = CalibrateCmd;
                    cmdType = "标定";
                    break;
                case 3:
                    cmd = MeaturePreCmd;
                    cmdType = "预览";
                    break;
                case 4:
                    cmd = MeatureInitCmd;
                    cmdType = "初始化测量";
                    break;
                case 5:
                    cmd = MeatureCmd;
                    cmdType = "测量";
                    break;
            }

            var err = new StringBuilder(1024);
            if (Dtu.do_send_user_data(station.Mobile, cmd, (uint)cmd.Length, err) == 0)
                MessageSend = "分站" + station.No + "发送" + cmdType + "开始命令成功";
            else
                MessageSend = "分站" + station.No + "发送"+ cmdType + "开始命令失败: " + err;
        }

        private DateTime ParseTime(IList<byte> src)
        {
            var time = (src[0]) | (src[1] << 8) | (src[2]) << 16 | (src[3]) << 24;
            return new DateTime(1970, 1, 1).AddSeconds(time).ToLocalTime();
        }

        private void UpdateSubstations()
        {
            var count = Dtu.get_online_user_amount();
            for (uint i = 0; i < count; ++i)
            {
                var userInfo = new UserInfo();
                Dtu.get_user_at(i, ref userInfo);

                var sub = SubstationList.FirstOrDefault(o => o.Mobile.Equals(userInfo.UserId));
                if (sub == null)
                {
                    MessageRecevied = @"收到未配置的分站信息，手机号为：" + userInfo.UserId;
                }
                else
                {
                    sub.LoginTime = userInfo.LoginDate;
                    sub.RefreshTime = ParseTime(userInfo.UpdateDate);
                    sub.Status = Substation.SubstationDict[sub.No].IsSampling ?
                        SubstationStatus.SamplingStarted : SubstationStatus.OnLine;
                }
            }
            for (var i = 0; i < SubstationList.Count; i++)
            {
                var sub = SubstationList[i];
                if (sub.RefreshTime.AddMinutes(5) >= DateTime.Now)
                    continue;
                sub.Status = SubstationStatus.OffLine;
            }
        }
    }
}
