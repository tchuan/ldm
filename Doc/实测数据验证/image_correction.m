function image_correction=image_correction(im_distortion,distx,disty)
%把畸变光斑图赋值到变量im上
im=double(im_distortion);
[x0, y0]=meshgrid(1:size(im,2),1:size(im,1));
%求得理想图各点在之前的畸变模型下在两个方向的畸变量
dx=distx(1)+distx(2)*x0+distx(3)*y0+distx(4)*x0.*x0+distx(5)*x0.*y0+distx(6)*y0.*y0+distx(7)*x0.*x0.*x0+distx(8)*x0.*x0.*y0+distx(9)*x0.*y0.*y0+distx(10)*y0.*y0.*y0;
dy=disty(1)+disty(2)*x0+disty(3)*y0+disty(4)*x0.*x0+disty(5)*x0.*y0+disty(6)*y0.*y0+disty(7)*x0.*x0.*x0+disty(8)*x0.*x0.*y0+disty(9)*x0.*y0.*y0+disty(10)*y0.*y0.*y0;
%坐标变换
x1=x0+dx;
y1=y0+dy;
%灰度差值
image_correction=griddata(x0,y0,im,x1,y1);
%把得到的校正图数据类型由double转化为8位灰度图
image_correction=uint8(image_correction);
imshow(image_correction);