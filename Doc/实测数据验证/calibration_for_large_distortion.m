function [distx, disty, magn]=calibration_for_large_distortion()

%% 选取初始坐标点
im_distortion = imread('C.BMP');
im_distortion = rgb2gray(im_distortion);
m = 9;
n = 12;
grid_length = 14.2/14*10000;

%% 选择初始计算点
imshow(im_distortion);
hold on;
[xxx, yyy]=getline();
plot(xxx,yyy,'ok','markerfacecolor','g')
wintx=round((xxx(2)-xxx(1))/3);
winty=wintx;

xd=reshape(xxx, n, m)';
yd=reshape(yyy.', n, m)';

%% 计算角点
%------------输入参数
I=double(im_distortion);

offx = (-wintx:wintx)'*ones(1, 2*winty+1);
offy = ones(2*wintx+1,1)*(-winty:winty);
mask = exp(-((-wintx:wintx)'/(wintx)).^2) * exp(-((-winty:winty)/(winty)).^2);
MaxIter = 10;
[nx,ny] = size(I);
%------------- 初始一列
xx = zeros(m, n);
yy = zeros(m, n);
for i=1:m
    for j=1:n
        xc(2)=xd(i,j);
        xc(1)=yd(i,j);
        
        compt = 0;                  % no iteration yet
        while   compt<MaxIter
            cIx = xc(1);            %
            cIy = xc(2);			% Coords. of the point
            crIx = round(cIx); 		% on the initial image
            crIy = round(cIy); 		%
            itIx = cIx - crIx; 		% Coefficients
            itIy = cIy - crIy; 		% to compute
            if itIx > 0, 			% the sub pixel
                vIx = [itIx 1-itIx 0]'; 	% accuracy.
            else
                vIx = [0 1+itIx -itIx]';
            end;
            if itIy > 0,
                vIy = [itIy 1-itIy 0];
            else
                vIy = [0 1+itIy -itIy];
            end;
            
            % What if the sub image is not in?
            if (crIx-wintx-2 < 1)
                xmin=1;
                xmax = 2*wintx+5;
            elseif (crIx+wintx+2 > nx)
                xmax = nx;
                xmin = nx-2*wintx-4;
            else
                xmin = crIx-wintx-2;
                xmax = crIx+wintx+2;
            end;
            if (crIy-winty-2 < 1)
                ymin=1; ymax = 2*winty+5;
            elseif (crIy+winty+2 > ny)
                ymax = ny;
                ymin = ny-2*winty-4;
            else
                ymin = crIy-winty-2;
                ymax = crIy+winty+2;
            end;
            SI = I(xmin:xmax,ymin:ymax); % The necessary neighborhood
            SI = conv2(SI,vIx,'same');
            SI = conv2(SI,vIy,'same');
            SI = SI(2:2*wintx+4,2:2*winty+4); % The subpixel interpolated neighborhood
            [gy,gx] = gradient(SI); 		% The gradient image
            gx = gx(2:2*wintx+2,2:2*winty+2); % extraction of the useful parts only
            gy = gy(2:2*wintx+2,2:2*winty+2); % of the gradients
            px = cIx + offx;
            py = cIy + offy;
            gxx = gx .* gx .* mask;
            gyy = gy .* gy .* mask;
            gxy = gx .* gy .* mask;
            bb = [sum(sum(gxx .* px + gxy .* py)); sum(sum(gxy .* px + gyy .* py))];
            a = sum(sum(gxx));
            b = sum(sum(gxy));
            c = sum(sum(gyy));
            dt = a*c - b^2;
            xc = [c*bb(1)-b*bb(2) a*bb(2)-b*bb(1)]/dt;
            compt = compt + 1;
        end
        yy(i,j) = xc(1);
        xx(i,j) = xc(2);
    end
end
xd=xx;
yd=yy;
I=uint8(I);
imshow(I)
hold on
plot(xd,yd,'ok','markerfacecolor','g')
%% 标准坐标
pl=floor(m/2)+1;
pr=floor(n/2)+1;
%得到畸变网格场中心节点的坐标
xm=xd(pl,pr);
ym=yd(pl,pr);
%理想网格场各相邻节点的高差和宽水平差
sy=((yd(pl+1,pr)-yd(pl-1,pr)))/2;
sx=((xd(pl,pr+1)-xd(pl,pr-1)))/2;
%求校正后图像的物面分辨率
magn=grid_length/((sx+sy)/2);
save('magn.mat', 'magn');
%生成均匀的理想网格节点坐标
[xs, ys]=meshgrid(xm-floor(n/2)*sx:sx:xm+(n-pr)*sx,ym-floor(m/2)*sy:sy:ym+(m-pl)*sy);
% plot(xs,ys,'ok','markerfacecolor','r')
%% 计算参数
%得到各节点的在两个方向上的畸变量
dx=xd-xs;
dy=yd-ys;
xd=xs(:);
yd=ys(:);
dx=dx(:);
dy=dy(:);
xd=xd';
yd=yd';
dx=dx';
dy=dy';
A=[ones(size(xd));xd;yd;xd.^2;xd.*yd;yd.^2;xd.^3;xd.^2.*yd;xd.*yd.^2;yd.^3];
%利用三次畸变模型求得两个方向上的畸变系数
distx=dx/A;
disty=dy/A;

image_correction(im_distortion, distx, disty);
save('para','distx','disty');


