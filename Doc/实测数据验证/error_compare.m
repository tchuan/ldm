close all
clear all
clc
%%
a2=imread('corrected_image_large.bmp');
figure
imshow(a2)
hold on
a2=double(a2);
%%
% for i=1:4,
%     [xx yy]=getline();
%     plot(xx,yy,'ok','markerfacecolor','g')
%     x(i)=xx;
%     y(i)=yy;
% end
x=1000*[0.0700    1.2080    1.2065    0.0715];
y=[67.2174   68.7167  975.7738  975.7738];
nx=19;ny=15;
ly=(y(4)-y(1))/ny;lx=(x(2)-x(1))/nx;
[Y X]=meshgrid(y(1):ly:y(4),x(1):lx:x(2));
wintx=round((x(2)-x(1))/nx/3);winty=round((y(4)-y(1))/ny/3);
X=X(:);Y=Y(:);
for i=1:size(X,1);
coord2(i,:)= cornerfinder([X(i);Y(i)],a2,winty,wintx);
end
plot(coord2(:,1),coord2(:,2),'ok','markerfacecolor','g')
cx_correction=reshape(coord2(:,1),nx+1,ny+1);cy_correction=reshape(coord2(:,2),nx+1,ny+1);
title('校正图像')
%%
m=16;n=20;
pl=floor(n/2)+1;
pr=floor(m/2)+1;
xm =cx_correction(pl,pr);
ym =cy_correction(pl,pr);

% xm =cx_correction(1,1);
% ym =cy_correction(1,1);
sx = 60.0681;sy = 60.8483;
[cx_standard cy_standard]=meshgrid(ym-(pr-1)*sy:sy:(n+1)*sy,xm-(pl-1)*sx:sx:(m+1)*sx);
% [cy_standard cx_standard]=meshgrid(ym:sx:(n+1)*sx,xm:sy:(m+1)*sy);
cy_standard=cy_standard';
cx_standard=cx_standard';
% xs=xs';ys=ys';
plot(cx_standard(:),cy_standard(:),'ok','markerfacecolor','r')
plot(xm,ym,'ok','markerfacecolor','m')
%%
figure
aex=abs(cx_correction-cx_standard);
aey=abs(cy_correction-cy_standard);
pcolor(cy_standard(2:end-1,2:end-1),cx_standard(2:end-1,2:end-1),0.5*aex(2:end-1,2:end-1))
title('x方向绝对误差','fontsize',14)
colorbar
xlabel('x(pixel)','fontsize',14)
ylabel('y(pixel)','fontsize',14)
set(gca,'fontsize',14)
%
figure
pcolor(cy_standard(2:end-1,2:end-1),cx_standard(2:end-1,2:end-1),0.5*aey(2:end-1,2:end-1))
title('y方向绝对误差','fontsize',14)
colorbar
xlabel('x(pixel)','fontsize',14)
ylabel('y(pixel)','fontsize',14)
set(gca,'fontsize',14)
%
figure
aa=((0.5*aey(2:end-1,2:end-1)).^2+(0.5*aex(2:end-1,2:end-1)).^2).^0.5;
pcolor(cy_standard(2:end-1,2:end-1),cx_standard(2:end-1,2:end-1),aa)
hold on
% bex=cx_correction-cx_standard;
% bey=cy_correction-cy_standard;
% quiver(cy_standard(2:end-1,2:end-1),cx_standard(2:end-1,2:end-1),0.5*bex(2:end-1,2:end-1),0.5*bey(2:end-1,2:end-1))
title('合误差场','fontsize',14)
colorbar
xlabel('x(pixel)','fontsize',14)
ylabel('y(pixel)','fontsize',14)
set(gca,'fontsize',14)
% %%
% figure
% rex=100*abs(cx_correction-cx_standard)./cx_standard;
% rey=100*abs(cy_correction-cy_standard)./cy_standard;
% pcolor(rex)
% title('x方向相对误差')
% colorbar
% figure
% pcolor(rey)
% title('y方向相对误差')
% colorbar
