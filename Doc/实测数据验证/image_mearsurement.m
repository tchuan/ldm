function [x y total] = image_mearsurement( image_correction ,magn)
%IMAGE_MEARSUREMENT Summary of this function goes here
%   Detailed explanation goes here
%求得校正后图像光斑的中心位置
    a=image_correction;
    %二值化滤去低灰度背景
    b=im2bw(a);
    total=sum(b(:));
    BW=double(a);
    %得到加权后的图
    BW=BW.*b;
    %灰度图总灰度
    sumb = sum(BW(:));
    [l,c] = size(BW);
    [ii,jj] = meshgrid(1:c, 1:l);
    %灰度图对Y轴的静矩
    sumii = sum(sum(ii.*BW));
    %灰度图对X轴的静矩
    sumjj = sum(sum(jj.*BW));
    %求得光斑的形心坐标
    x= (sumii/sumb-xo)*magn;
    y= (sumjj/sumb-yo)*magn;

end

