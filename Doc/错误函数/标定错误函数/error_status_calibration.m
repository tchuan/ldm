% function error_code=error_status_calibration(corrected_image,threshold,m,n)
%%
close all
clear all
clc
corrected_image=imread('b.bmp');
n=15-1;m=11-1;
%%
imshow(corrected_image)
hold on

% for i=1:4,
%     [xx yy]=getline();
%     plot(xx,yy,'ok','markerfacecolor','g')
%     x(i)=xx;
%     y(i)=yy;
% end
x=[83.0000  901.0000  911.0000   79.0000];
y=[78 86 664 666];

ly=(y(4)-y(1))/m;
lx=(x(2)-x(1))/n;

[Y X]=meshgrid(y(1):ly:y(4),x(1):lx:x(2));
wintx=round((x(2)-x(1))/m/3);winty=round((y(4)-y(1))/n/3);
X=X(:);Y=Y(:);
%% �ǵ�ʶ��
line_feat=1;
I=double(corrected_image);
wintx=round(lx/3);
winty=round(ly/3);
offx = [-wintx:wintx]'*ones(1,2*winty+1);
offy = ones(2*wintx+1,1)*[-winty:winty];
mask = exp(-((-wintx:wintx)'/(wintx)).^2) * exp(-((-winty:winty)/(winty)).^2);
resolution = 0.005;
MaxIter = 10;
[nx,ny] = size(I);
% for i=1:size(X,1);
%     v_extra = resolution + 1; 		% just larger than resolution
%     compt = 0; 				% no iteration yet
%     while (norm(v_extra) > resolution) & (compt<MaxIter),
%         cIx = X(i);		%
%         cIy = Y(i);			% Coords. of the point
%         crIx = round(cIx); 		% on the initial image
%         crIy = round(cIy); 		%
%         itIx = cIx - crIx; 		% Coefficients
%         itIy = cIy - crIy; 		% to compute
%         if itIx > 0, 			% the sub pixel
%             vIx = [itIx 1-itIx 0]'; 	% accuracy.
%         else
%             vIx = [0 1+itIx -itIx]';
%         end;
%         if itIy > 0,
%             vIy = [itIy 1-itIy 0];
%         else
%             vIy = [0 1+itIy -itIy];
%         end;
%         
%         % What if the sub image is not in?
%         if (crIx-wintx-2 < 1), xmin=1; xmax = 2*wintx+5;
%         elseif (crIx+wintx+2 > nx), xmax = nx; xmin = nx-2*wintx-4;
%         else
%             xmin = crIx-wintx-2; xmax = crIx+wintx+2;
%         end;
%         if (crIy-winty-2 < 1), ymin=1; ymax = 2*winty+5;
%         elseif (crIy+winty+2 > ny), ymax = ny; ymin = ny-2*winty-4;
%         else
%             ymin = crIy-winty-2; ymax = crIy+winty+2;
%         end;
%         SI = I(xmin:xmax,ymin:ymax); % The necessary neighborhood
%         SI = conv2(conv2(SI,vIx,'same'),vIy,'same');
%         SI = SI(2:2*wintx+4,2:2*winty+4); % The subpixel interpolated neighborhood
%         [gy,gx] = gradient(SI); 		% The gradient image
%         gx = gx(2:2*wintx+2,2:2*winty+2); % extraction of the useful parts only
%         gy = gy(2:2*wintx+2,2:2*winty+2); % of the gradients
%         px = cIx + offx;
%         py = cIy + offy;
%         gxx = gx .* gx .* mask;
%         gyy = gy .* gy .* mask;
%         gxy = gx .* gy .* mask;
%         bb = [sum(sum(gxx .* px + gxy .* py)); sum(sum(gxy .* px + gyy .* py))];
%         a = sum(sum(gxx));
%         b = sum(sum(gxy));
%         c = sum(sum(gyy));
%         dt = a*c - b^2;
%         xc2 = [c*bb(1)-b*bb(2) a*bb(2)-b*bb(1)]/dt;
%         compt = compt + 1;
%         
%     end
% xc(i,:) = xc2;
% end
% % xc = fliplr(xc);
% xc = xc';
%%
for i=1:size(X,1);
coord(i,:)= cornerfinder([X(i);Y(i)],I,winty,wintx);
end
% plot(X,Y,'ok','markerfacecolor','r')
plot(coord(:,1),coord(:,2),'ok','markerfacecolor','g')
cx=reshape(coord(:,1),m+1,n+1);
cy=reshape(coord(:,2),m+1,n+1);
%%
dx=diff(cx);
dy=diff(cy);
s1=std(dx(:));
s2=std(dy(:));
% %%
% if percent<p
%    error_code=1; 
% else
%     error_code=0; 
% end