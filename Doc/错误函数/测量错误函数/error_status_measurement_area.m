function error_code=error_status_measurement_area(image,threshold,p,area)

bw=im2bw(image,threshold);
[x y]=find(bw==1);
area_new=size(x);
percent=area_new/area;
if percent<p
   error_code=1; 
else
    error_code=0; 
end