function error_code=error_status_measurement_location(xc,yc,total,para_location)

xL=para_location(1);
xR=para_location(2);
yL=para_location(3);
yR=para_location(4);
ratio=total/norm_total;

if xc<xL||xc>xR||yc<yL||yc>yR||ratio<0.3
    error_code=1;
else
    error_code=0;
end